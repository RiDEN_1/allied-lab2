package task3;

import java.io.File;
import java.io.IOException;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

public class SerializeToJson {

    public static void main(String[] args) throws JsonGenerationException, JsonMappingException, IOException {

        ObjectMapper mapper = new ObjectMapper();

        Car car = new Car();
        car.setName("Acura RSX");
        car.setWeight(1100);
        car.setMsrp(new Price(10000, "dollar"));
        car.setSelling(new Price(5000, "dollar"));

        String[] mods = {"turbo", "intercooler", "brake system", "spoiler", "low profile tyres"};
        car.setMods(mods);

        //Convert object to JSON string and save into file directly
        mapper.writeValue(new File("car.json"), car);

        //Convert object to JSON string
        String jsonInString = mapper.writeValueAsString(car);
        System.out.println(jsonInString+"\n");

        //Convert object to JSON string and pretty print
        jsonInString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(car);
        System.out.println(jsonInString);

    }
}