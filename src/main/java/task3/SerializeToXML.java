package task3;

import com.thoughtworks.xstream.XStream;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class SerializeToXML {

    private static final String SERIALIZED_FILE_NAME = "car.xml";

    public static void main(String[] args) throws IOException {
        Car car = new Car();
        car.setName("Volvo XC90");
        car.setWeight(350);
        car.setMsrp(new Price(30000, "dollar"));
        car.setSelling(new Price(15000, "dollar"));

        String[] mods = {"supercharger", "intercooler", "sport springs", "carbon brakes", "titanium exhaust", "forged wheels"};
        car.setMods(mods);

        XStream xStream = getXstreamObject();
        String xml = xStream.toXML(car);
        System.out.println(xml);

        try {
            FileOutputStream fs = new FileOutputStream(SERIALIZED_FILE_NAME);
            xStream.toXML(car, fs);
            System.out.println();
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        }
    }

    private static XStream getXstreamObject() {
        XStream xstream = new XStream();
        return xstream;
    }
}