package task3;

public class Price {
    private int value;
    private String currency;

    public Price() {}

    public Price(int value, String currency) {
        super();
        this.value = value;
        this.currency = currency;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

}