package task3;

import java.io.File;
import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class DeserializeFromJson  {

        public static void main(String[] args) {
            DeserializeFromJson obj = new DeserializeFromJson();
            obj.run();
        }

        private void run() {
            ObjectMapper mapper = new ObjectMapper();

            try {
                // Convert JSON string from file to Object
                Car car = mapper.readValue(new File("D:\\University\\6semester\\APPOO\\LAB3\\car.json"), Car.class);

                //Pretty print
                String prettyCar = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(car);
                System.out.println(prettyCar);

            } catch (JsonGenerationException e) {
                e.printStackTrace();
            } catch (JsonMappingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
}

