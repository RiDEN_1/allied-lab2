package task3;

public class Car {
    private String name;
    private int weight;
    private Price msrp;
    private Price selling;
    private String[] mods;

    public Car() {}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public Price getMsrp() {
        return msrp;
    }

    public void setMsrp(Price msrp) {
        this.msrp = msrp;
    }

    public Price getSelling() {
        return selling;
    }

    public void setSelling(Price selling) {
        this.selling = selling;
    }

    public String[] getMods() {
        return mods;
    }

    public void setMods(String[] mods) {
        this.mods = mods;
    }

}