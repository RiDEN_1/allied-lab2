package task3;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

import java.io.*;

public class DeserializeFromXML {

    private static final String SERIALIZED_FILE_NAME = "car.xml";

    public static void main(String[] args) throws IOException {
        XStream xStream = new XStream(new DomDriver());
        Car car = new Car();

        try {
            FileInputStream fs = new FileInputStream(SERIALIZED_FILE_NAME);
            xStream.fromXML(fs, car);
            System.out.println(car.toString());

        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
    }
}