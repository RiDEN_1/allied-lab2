package task2;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;

public class JsonRead {

    @SuppressWarnings("unchecked")
    public static void main(String[] args)
    {
        //JSON parser object to parse read file
        JSONParser jsonParser = new JSONParser();

        try (FileReader reader = new FileReader("D:\\University\\Allied Testing\\LAB2\\src\\main\\java\\task2\\UTMAuto.json"))
        {
            //Read JSON file
            Object obj = jsonParser.parse(reader);

            JSONArray carList = (JSONArray) obj;
            System.out.println(carList);

            //Iterate over car array
            carList.forEach( emp -> parseCarObject( (JSONObject) emp ) );

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private static void parseCarObject(JSONObject employee)
    {
        //Get car object within list
        JSONObject carObject = (JSONObject) employee.get("stadium");

        //Get car name
        String name = (String) carObject.get("name");
        System.out.println(name);

        // loop array
        JSONArray msg1 = (JSONArray) carObject.get("modslist");
        Iterator<String> iterator1 = msg1.iterator();
        while (iterator1.hasNext()) {
            System.out.println(iterator1.next());
        }
    }
}