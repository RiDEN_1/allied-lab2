package task2;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.FileWriter;
import java.io.IOException;

public class JsonCreateFile {

    public static void main(String[] args) {


        JSONObject carDetails1 = new JSONObject();
        carDetails1.put("name", "Acura");

        JSONArray modsArray1 = new JSONArray();
        modsArray1.add("turbo");
        modsArray1.add("intercooler");
        modsArray1.add("brakes");

        carDetails1.put("modlist", modsArray1);

        carDetails1.put("country", "Japan");
        carDetails1.put("power", "170HP");

        JSONObject carObject1 = new JSONObject();
        carObject1.put("car", carDetails1);


        JSONObject carDetails2 = new JSONObject();
        carDetails2.put("name", "Volvo");

        JSONArray modsArray2 = new JSONArray();
        modsArray2.add("supercharger");
        modsArray2.add("spoiler");
        modsArray2.add("nitrous");

        carDetails2.put("modlist", modsArray1);

        carDetails2.put("country", "Sweden");
        carDetails2.put("power", "240HP");

        JSONObject carObject2 = new JSONObject();
        carObject2.put("car", carDetails2);



        JSONObject carDetails3 = new JSONObject();
        carDetails3.put("name", "Chevrolet");

        JSONArray modsArray3 = new JSONArray();
        modsArray3.add("racing wheels");
        modsArray3.add("brakes");
        modsArray3.add("leather seats");

        carDetails3.put("modlist", modsArray1);

        carDetails3.put("country", "USA");
        carDetails3.put("power", "250HP");

        JSONObject carObject3 = new JSONObject();
        carObject3.put("car", carDetails3);



        JSONObject carDetails4 = new JSONObject();
        carDetails4.put("name", "Dodge");

        JSONArray modsArray4 = new JSONArray();
        modsArray4.add("hypercharger");
        modsArray4.add("intercooler");
        modsArray4.add("spoler");

        carDetails4.put("modlist", modsArray1);

        carDetails4.put("country", "USA");
        carDetails4.put("power", "120HP");

        JSONObject carObject4 = new JSONObject();
        carObject4.put("car", carDetails4);


        JSONObject carDetails5 = new JSONObject();
        carDetails5.put("name", "Renault");

        JSONArray modsArray5 = new JSONArray();
        modsArray5.add("sleeks");
        modsArray5.add("wheelie bar");
        modsArray5.add("blowoff");

        carDetails5.put("modlist", modsArray1);

        carDetails5.put("country", "France");
        carDetails5.put("power", "100HP");

        JSONObject carObject5 = new JSONObject();
        carObject5.put("car", carDetails5);


        //Add cars to list
        JSONArray carList = new JSONArray();
        carList.add(carObject1);
        carList.add(carObject2);
        carList.add(carObject3);
        carList.add(carObject4);
        carList.add(carObject5);

        //Write JSON file
        try (FileWriter file = new FileWriter("D:\\University\\Allied Testing\\LAB2\\src\\main\\java\\task2\\UTMAuto.json")) {

            file.write(carList.toJSONString());
            file.flush();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}