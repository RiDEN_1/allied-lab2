package task1.DOM;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.File;

public class QueryXmlFile {

    public static void main(String argv[]) {

        try {
            File inputFile = new File("D:\\University\\Allied Testing\\LAB2\\src\\main\\java\\task1\\DOM\\race.xml");
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(inputFile);
            doc.getDocumentElement().normalize();
            System.out.print("Root element: ");
            System.out.println(doc.getDocumentElement().getNodeName());
            NodeList nList = doc.getElementsByTagName("car");
            System.out.println("----------------------------");

            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                System.out.println("\nCurrent Element :");
                System.out.print(nNode.getNodeName());

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    System.out.print("name : ");
                    System.out.println(eElement.getAttribute("name"));
                    NodeList carColorList = eElement.getElementsByTagName("color");
                    NodeList carPowerList = eElement.getElementsByTagName("power");

                    for (int count = 0; count < carColorList.getLength(); count++) {
                        Node node = carColorList.item(count);

                        if (node.getNodeType() == node.ELEMENT_NODE) {
                            Element car = (Element) node;
                            System.out.print("Color : ");
                            System.out.println(car.getTextContent());
                        }
                    }

                    for (int count = 0; count < carPowerList.getLength(); count++) {
                        Node node = carPowerList.item(count);

                        if (node.getNodeType() == node.ELEMENT_NODE) {
                            Element car = (Element) node;
                            System.out.print("Power : ");
                            System.out.println(car.getTextContent());
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}