package task1.DOM;

import java.io.File;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;

public class ParseXmlFile {

    public static void main(String[] args) {

        try {
            File inputFile = new File("D:\\University\\Allied Testing\\LAB2\\src\\main\\java\\task1\\DOM\\race.xml");
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(inputFile);
            doc.getDocumentElement().normalize();
            System.out.println("Root element : " + doc.getDocumentElement().getNodeName());
            NodeList nList = doc.getElementsByTagName("car");
            System.out.println("----------------------------");

            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                System.out.println("\nCurrent Element : " + nNode.getNodeName());

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    System.out.println("Car name : "
                            + eElement.getAttribute("name"));
                    System.out.println("Color : "
                            + eElement
                            .getElementsByTagName("color")
                            .item(0)
                            .getTextContent());
                    System.out.println("Type : "
                            + eElement
                            .getElementsByTagName("type")
                            .item(0)
                            .getTextContent());
                    System.out.println("Country : "
                            + eElement
                            .getElementsByTagName("country")
                            .item(0)
                            .getTextContent());
                    System.out.println("Power : "
                            + eElement
                            .getElementsByTagName("power")
                            .item(0)
                            .getTextContent());
                    System.out.println("Price : "
                            + eElement
                            .getElementsByTagName("price")
                            .item(0)
                            .getTextContent());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}