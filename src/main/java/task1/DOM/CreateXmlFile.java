package task1.DOM;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class CreateXmlFile {

    public static void main(String[] args) {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder;
        try {
            dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.newDocument();
            //add elements to Document
            Element rootElement = doc.createElement("race");
            //append root element to document
            doc.appendChild(rootElement);

            //append first child element to root element
            rootElement.appendChild(getCar(doc, "Acura", "blue", "coupe", "Japan",
                    "170 HP", "5000$"));

            //append second child
            rootElement.appendChild(getCar(doc, "Volvo", "silver", "SUV", "Sweden",
                    "220 HP", "13000$"));

            //append third child
            rootElement.appendChild(getCar(doc, "Chevrolet", "dark blue", "SUV", "USA",
                    "250 HP", "9000$"));

            //append fourth child
            rootElement.appendChild(getCar(doc, "Chrysler", "silver", "universal", "USA",
                    "120 HP", "5000$"));

            //append fifth child
            rootElement.appendChild(getCar(doc, "Lamborghini", "verde mantis", "coupe", "Italy",
                    "610 HP", "350000$"));

            //append sixth child
            rootElement.appendChild(getCar(doc, "Porsche", "black", "coupe", "Germany",
                    "550 HP", "210000$"));


            //for output to file, console
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            //for pretty print
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            DOMSource source = new DOMSource(doc);

            //write to console or file
            StreamResult console = new StreamResult(System.out);
            StreamResult file = new StreamResult(new File("D:\\University\\Allied Testing\\LAB2\\src\\main\\java\\task1\\DOM\\race.xml"));

            //write data
            transformer.transform(source, console);
            transformer.transform(source, file);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private static Node getCar(Document doc, String name, String color, String type, String country,
                               String power, String price) {
        Element car = doc.createElement("car");

        //set name attribute
        car.setAttribute("name", name);

        //create color element
        car.appendChild(getCarElements(doc, car, "color", color));

        //create type element
        car.appendChild(getCarElements(doc, car, "type", type));

        //create country element
        car.appendChild(getCarElements(doc, car, "country", country));

        //create power element
        car.appendChild(getCarElements(doc, car, "power", power));

        //create price element
        car.appendChild(getCarElements(doc, car, "price", price));

        return car;
    }


    //utility method to create text node
    private static Node getCarElements(Document doc, Element element, String name, String value) {
        Element node = doc.createElement(name);
        node.appendChild(doc.createTextNode(value));
        return node;
    }

}
