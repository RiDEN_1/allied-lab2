package task1.SAX;

public class F1GrandPrix {
    private int year;
    private String country;
    private String team;
    private String winningdriver;
    private String fastestlapdriver;

    public F1GrandPrix(int year, String country, String team, String winningdriver, String fastestlapdriver)    {
        this.year = year;
        this.country = country;
        this.team = team;
        this.winningdriver = winningdriver;
        this.fastestlapdriver = fastestlapdriver;
    }

    public int getYear() {
        return year;
    }
    public void setYear(int year) {
        this.year = year;
    }
    public String getCountry() {
        return country;
    }
    public void setCountry(String country) {
        this.country = country;
    }
    public String getTeam() {
        return team;
    }
    public void setTeam(String team) {
        this.team = team;
    }

    public String getWinningdriver() {
        return winningdriver;
    }
    public void setWinningdriver(String winningdriver) {
        this.winningdriver = winningdriver;
    }

    public String getFastestlapdriver() {
        return fastestlapdriver;
    }
    public void setFastestlapdriver(String fastestlapdriver) {
        this.fastestlapdriver = fastestlapdriver;
    }


    @Override
    public String toString() {
        return "winner:: year="+this.year+" country=" + this.country + " team=" + this.team + " winningdriver=" + this.winningdriver +
                " fastestlapdriver=" + this.fastestlapdriver;
    }

}