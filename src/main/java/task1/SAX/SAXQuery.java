package task1.SAX;

import java.io.File;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

public class SAXQuery {

    public static void main(String[] args) {

        try {
            File inputFile = new File("D:\\University\\Allied Testing\\LAB2\\src\\main\\java\\task1\\SAX\\F1GP.xml");
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();
            UserHandlerQuery userhandlerquery = new UserHandlerQuery();
            saxParser.parse(inputFile, userhandlerquery);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}