package task1.SAX;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class UserHandlerQuery extends DefaultHandler {

    boolean bYear = false;
    boolean bTeam = false;
    boolean bWinning = false;
    boolean bFastest = false;
    String Country = null;

    @Override
    public void startElement(String uri,
                             String localName, String qName, Attributes attributes)
            throws SAXException {

        if (qName.equalsIgnoreCase("host")) {
            Country = attributes.getValue("country");
        }
        if(("China").equals(Country) && qName.equalsIgnoreCase("host")) {
            System.out.println("Start Element :" + qName);
        }
        if (qName.equalsIgnoreCase("year")) {
            bYear = true;
        } else if (qName.equalsIgnoreCase("team")) {
            bTeam = true;
        } else if (qName.equalsIgnoreCase("winningdriver")) {
            bWinning = true;
        } else if (qName.equalsIgnoreCase("fastestdriver")) {
            bFastest = true;
        }
    }

    @Override
    public void endElement(
            String uri, String localName, String qName) throws SAXException {

        if (qName.equalsIgnoreCase("host")) {
            if(("China").equals(Country) && qName.equalsIgnoreCase("host"))
                System.out.println("End Element :" + qName);
        }
    }

    @Override
    public void characters(char ch[], int start, int length) throws SAXException {

        if (bYear && ("China").equals(Country)) {
            System.out.println("Year: " + new String(ch, start, length));
            bYear = false;
        } else if (bTeam && ("China").equals(Country)) {
            System.out.println("Team: " + new String(ch, start, length));
            bTeam = false;
        } else if (bWinning && ("China").equals(Country)) {
            System.out.println("Winning: " + new String(ch, start, length));
            bWinning = false;
        } else if (bFastest && ("China").equals(Country)) {
            System.out.println("Fastest: " + new String(ch, start, length));
            bFastest = false;
        }
    }
}
