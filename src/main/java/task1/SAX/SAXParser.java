package task1.SAX;

import javax.xml.parsers.SAXParserFactory;
import java.io.File;

public class SAXParser {

    public static void main(String[] args) {

        try {
            File inputFile = new File("D:\\University\\Allied Testing\\LAB2\\src\\main\\java\\task1\\SAX\\F1GP.xml");
            SAXParserFactory factory = SAXParserFactory.newInstance();
            javax.xml.parsers.SAXParser saxParser = factory.newSAXParser();
            UserHandler userhandler = new UserHandler();
            saxParser.parse(inputFile, userhandler);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
