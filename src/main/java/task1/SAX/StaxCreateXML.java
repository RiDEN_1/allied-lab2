package task1.SAX;

import com.sun.xml.txw2.output.IndentingXMLStreamWriter;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.stream.*;

public class StaxCreateXML {

    public static void main(String[] args){
        F1GrandPrix winner1 = new F1GrandPrix(2017,"China", "Mercedes","Lewis Hamilton", "Lewis Hamilton");
        F1GrandPrix winner2 = new F1GrandPrix(2017,"Australia", "Ferrari", "Sebastian Vettel", "Kimi Raikkonen");
        F1GrandPrix winner3 = new F1GrandPrix(2017,"Bahrain", "Ferrari", "Sebastian Vettel","Lewis Hamilton");
        F1GrandPrix winner4 = new F1GrandPrix(2017,"Russia", "Mercedes","Valtteri Bottas", "Kimi Raikkonen");
        F1GrandPrix winner5 = new F1GrandPrix(2017,"Spain","Mercedes", "Lewis Hamilton", "Lewis Hamilton");
        F1GrandPrix winner6 = new F1GrandPrix(2017,"Monaco", "Ferrari", "Sebastian Fettel", "Sergio Perez");
        F1GrandPrix winner7 = new F1GrandPrix(2017,"Canada", "Mercedes", "Lewis Hamilton", "Lewis Hamilton");
        F1GrandPrix winner8 = new F1GrandPrix(2017,"Azerbaijan", "RBR", "Daniel Ricciardo", "Sebastian Vettel");

        List<F1GrandPrix> f1GP_list = new ArrayList<F1GrandPrix>();
        f1GP_list.add(winner1);
        f1GP_list.add(winner2);
        f1GP_list.add(winner3);
        f1GP_list.add(winner4);
        f1GP_list.add(winner5);
        f1GP_list.add(winner6);
        f1GP_list.add(winner7);
        f1GP_list.add(winner8);

        stAXToXml(f1GP_list);
    }

    public static void stAXToXml(List<F1GrandPrix> list) {

        try {
            if (null != list && !list.isEmpty()) {
                XMLOutputFactory xof = XMLOutputFactory.newInstance();
                XMLStreamWriter xmlsw = new IndentingXMLStreamWriter(xof.createXMLStreamWriter(new FileWriter("D:\\University\\Allied Testing\\LAB2\\src\\main\\java\\task1\\SAX\\F1GP.xml")));

                xmlsw.writeStartDocument("UTF-8", "1.0");
                xmlsw.writeStartElement("F1GrandPrix");

                for (F1GrandPrix po : list) {

                    xmlsw.writeStartElement("host");
                    xmlsw.writeAttribute("country", po.getCountry() );

                    xmlsw.writeStartElement("year");
                    xmlsw.writeCharacters(String.valueOf(po.getYear()));
                    xmlsw.writeEndElement();

                    xmlsw.writeStartElement("team");
                    xmlsw.writeCharacters(po.getTeam());
                    xmlsw.writeEndElement();

                    xmlsw.writeStartElement("winningdriver");
                    xmlsw.writeCharacters(po.getWinningdriver());
                    xmlsw.writeEndElement();

                    xmlsw.writeStartElement("fastestdriver");
                    xmlsw.writeCharacters(po.getFastestlapdriver());
                    xmlsw.writeEndElement();

                    xmlsw.writeEndElement();
                }

                xmlsw.writeEndElement();
                xmlsw.writeEndDocument();
                xmlsw.flush();
                xmlsw.close();
            }

        } catch (XMLStreamException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}