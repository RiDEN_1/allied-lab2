package task1.SAX;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class UserHandler extends DefaultHandler {

    boolean bYear = false;
    boolean bTeam = false;
    boolean bWinning = false;
    boolean bFastest = false;

    @Override
    public void startElement(String uri,
                             String localName, String qName, Attributes attributes) throws SAXException {

        if (qName.equalsIgnoreCase("host")) {
            String Country = attributes.getValue("country");
            System.out.println("Country : " + Country);
        } else if (qName.equalsIgnoreCase("year")) {
            bYear = true;
        } else if (qName.equalsIgnoreCase("team")) {
            bTeam = true;
        } else if (qName.equalsIgnoreCase("winningdriver")) {
            bWinning = true;
        } else if (qName.equalsIgnoreCase("fastestdriver")) {
            bFastest = true;
        }
    }

    @Override
    public void endElement(String uri,
                           String localName, String qName) throws SAXException {
        if (qName.equalsIgnoreCase("flower")) {
            System.out.println("End Element :" + qName);
        }
    }

    @Override
    public void characters(char ch[], int start, int length) throws SAXException {

        if (bYear) {
            System.out.println("Year: " + new String(ch, start, length));
            bYear = false;
        } else if (bTeam) {
            System.out.println("Team: " + new String(ch, start, length));
            bTeam = false;
        } else if (bWinning) {
            System.out.println("Winning Driver: " + new String(ch, start, length));
            bWinning = false;
        } else if (bFastest) {
            System.out.println("Fastest Driver: " + new String(ch, start, length));
            bFastest = false;
        }
    }
}